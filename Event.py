#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 24 12:06:46 2019

@author: jamesmurray
"""

import numpy as np
from SEM import SEM

c = 3.*10**8              # [m/s] Speed of light
f_0= 430.*10**6           # [Hz]  Center Frequency of the Radar
wavelength = c/f_0

class Event:
    
    def __init__(self, observation, event):
        self.Valid_Flag = False
        self.Observations = []
        self.event = event
        self.add_observation(observation)
        # Set by Measurer based on Principle in Event (highest SNR Observation)
        self.Obs_time = np.nan
        self.Range = np.nan
        self.Doppler = np.nan
        self.Azimuth = np.nan
        self.Elevation = np.nan
        self.SNR_PP = np.nan
        self.SNR_OP = np.nan
        #self.Altitude = np.nan
        # Based on All Observations in an Event
        self.Avg_RCS_PP = np.nan
        self.Avg_RCS_OP = np.nan
        self.Total_RCS = np.nan
        self.Size_estimate = np.nan
        self.Start_PRI = np.nan
        self.End_PRI = np.nan
        self.Start_time = np.nan
        self.End_time = np.nan
    
    def add_observation(self, observation):
        observation.Event = self.event
        self.Observations.append(observation)
        
    def close_Event(self, Measurer):
        for obs in self.Observations:
            Measurer.measure(obs)
        self.Start_PRI = self.Observations[0].PRI
        self.End_PRI = self.Observations[-1].PRI
        self.Start_time = self.Observations[0].Time
        self.End_time = self.Observations[-1].Time
        self._set_Event_params()
        
    def _set_Event_params(self):
        SNR = []
        
        self.Avg_RCS_PP = 0.
        self.Avg_RCS_OP = 0.
        self.Total_RCS = 0.
        
        for obs in self.Observations:
            SNR.append(obs.SNR_PP)
            self.Avg_RCS_PP = self.Avg_RCS_PP + obs.RCS_PP
            self.Avg_RCS_OP = self.Avg_RCS_OP + obs.RCS_OP
            self.Total_RCS = self.Total_RCS + obs.RCS_TOT
            
        self.Avg_RCS_PP = self.Avg_RCS_PP/len(self.Observations)
        self.Avg_RCS_OP = self.Avg_RCS_OP/len(self.Observations)
        self.Total_RCS = self.Total_RCS/len(self.Observations)
        self.Size_Estimate = SEM(self.Total_RCS, wavelength)
            
        principle_index = np.argmax(SNR)
        
        self.Obs_time = self.Observations[principle_index].Time
        self.Range = self.Observations[principle_index].Range
        self.Doppler = self.Observations[principle_index].Doppler
        self.Azimuth = self.Observations[principle_index].Az_Enc
        self.Elevation = self.Observations[principle_index].El_Enc
        self.SNR_PP = self.Observations[principle_index].SNR_PP
        self.SNR_OP = self.Observations[principle_index].SNR_OP
    