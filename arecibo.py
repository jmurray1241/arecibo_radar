from numpy import correlate,abs,fft,zeros,size, arange, append,log10, conj, max, argmax, min, mean
from scipy.signal import butter, lfilter, bessel
import rdvme
import rcs
import sys

'''
Make sure to have rcs.py and rdvme.py in the same directory as arecibo.py.

Written by James Murray 2016.
'''


baud_rate = 2*10**-6      # [s]   This is the time between samples
sample_rate = 1/baud_rate # [samples/s] 
c = 3.*10**8              # [m/s] Speed of light
f_0= 430.*10**6           # [Hz]  Center Frequency of the Radar
start_of_pulse = 7        # [samples] sample at which the pulse begins
pulse_length = 880        # [samples]
receive_length = 10960    # [samples]
calibration_noise_power = 6.624*10**-16 # [W]
calibration_noise_length = 560 # [samples]


def cor(buf, beam = 1):
    '''
    # ######################### cor() ########################## #
    # This is a core function, which implements the match filter #
    # Pass this function a full data buffer and select a beam    #
    # Returns the matched-filtered time series of only that beam #
    # ########################################################## #
    '''
    if beam == 1:
        template = buf[start_of_pulse : start_of_pulse+pulse_length]
        return correlate(buf[:receive_length],template,'same')/receive_length
    elif beam == 2:
        template = buf[receive_length+start_of_pulse : receive_length+start_of_pulse+pulse_length]
        return correlate(buf[receive_length:2*receive_length],template,'same')/receive_length
    

def samp2range(samples,match_filtered=True):
    '''
    # ########### samp2range() ########## #
    # Pass this function a sample number  #
    # and it returns a range in meters    #
    # It also accounts for filter offset  #
    # ################################### #
    '''
    center2edge_offset = pulse_length/2.
    if not match_filtered:
        return (samples-start_of_pulse)*c/(2*sample_rate)
    if match_filtered:
        return (samples-center2edge_offset)*c/(2*sample_rate)


def rangeVrange_rate(buf,step=137,fft_len=1096,beam=1):
    '''
    # ############# rangeVrange_rate() ##################### #
    # Pass this function a data buffer and it will return a  #
    # range v. range-rate plot where each vertical slice is  #
    # an fft of length fft_len zero padded to a total length #
    # of 8 times fft_len. Each fft is taken by sliding over  #
    # step number of samples such that the overlap between   #
    # adjacent ffts is fft_len minus step.                   #
    # ###################################################### #

    '''
    padding=fft_len*7
    fft_total_len=fft_len+padding
    
    buf=cor(buf,beam)

    r_rdot=zeros(((fft_total_len)/2+1,(size(buf)-fft_len)/step))
    
    for i in arange((size(buf)-fft_len)/step):
        r_rdot[:,i]=abs(fft.rfft(append(buf[i*step:i*step+fft_len],zeros(padding)))/(8*fft_len))**2
    return r_rdot


def pulse2pulse(data_file,pulses=50,beam=1):
    '''
    # ############### pulse2pulse() ##################### #
    # Given a data file object, this function will return #
    # a 2 dimensional array with pulses number of pulses  #
    # ################################################### # 
    '''
    pulse=zeros((receive_length,pulses))
    if beam == 1:
        for i in arange(pulses):
            pulse[:,i]=data_file.buf[:receive_length]
            data_file.rdvmerec()
    elif beam == 2:
        for i in arange(pulses):
            pulse[:,i]=data_file.buf[receive_length:]
            data_file.rdvmerec()
            
    return pulse


def bessel_lowpass_filter(data, cutoff, order=5):
    '''
    # ############ bessel_lowpass_filter() ############## #
    # The bessel filter is a minimum group delay filter.  #
    # Given a time series of data and a cutoff frequency  #
    # in Hertz, it will return the low pass filtered data #
    # ################################################### #    
    '''
    nyq = 0.5 * sample_rate
    normal_cutoff = cutoff / nyq
    b, a = bessel(order, normal_cutoff, btype='low', analog=False)
    y = lfilter(b, a, data)

    return y

def butter_lowpass_filter(data, cutoff, order=5):
    '''
    # ############ butter_lowpass_filter() ############## #
    # The butterworth filter is a minimum ripple filter.  #
    # Given a time series of data and a cutoff frequency  #
    # in Hertz, it will return the low pass filtered data #
    # ################################################### #    
    '''
    nyq = 0.5 * sample_rate
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = lfilter(b, a, data)
    return y


def butter_bandpass_filter(data, lowcut, highcut, order=5):
    '''
    # ############ butter_bandpass_filter() ############## #
    # The butterworth filter is a minimum ripple filter.   #
    # Given a time series of data and cutoff frequencies   #
    # in Hertz, it will return the band pass filtered data #
    # #################################################### #    
    '''
    nyq = 0.5 * sample_rate
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    y = lfilter(b, a, data)
    return y


def vdop(bin,num_bins):
    '''
    # #################### vdop() ############### #
    # Given the doppler bin and the number points #
    # used to compute the fft, it will return the #
    # doppler velocity in meters per second       #
    # ########################################### #
    '''
    fdop=(bin/(num_bins*baud_rate)-1/(2*baud_rate))/2
    v=c*fdop/f_0/2
    return v
    
def envelope(data, filt=10**3,beam=1):
    '''
    # ############### envelope() ################## #
    # Given a data buffer, it will return a square  #
    # law detector evelope, which is obtained by    #
    # squaring post-match filtered data and passing #
    # it through a low pass filter.                 #
    # ############################################# #
    '''
    squared=bessel_lowpass_filter(cor(data,beam)**2,filt)
    return squared

def range_time(df,pulses,filt=10**3,normalized=False,beam=1):
    '''
    # ############# range_time() ############### #
    # Given a file object, it will return a bank #
    # of envelope detected pulses. If normalized #
    # is set to True, it will return each pulse  #
    # normalized by the maximum correlation.     #
    # ########################################## #
    '''
    
    envelopes=zeros((10960,pulses))
    
    for pulse in arange(pulses):
        df.rdvmerec()
        env=envelope(df.buf,filt,beam)
        if normalized:
            envelopes[:,pulse]=env/max(env)
        elif  not normalized:
            envelopes[:,pulse]=env
        
    return envelopes



def whitening_model(data_file,beam,pulses_for_average=1000):
    '''
    # ############### whitening_model() ############## #
    # This takes a file object and returns a whitening #
    # array which flattens the noise floor of the      #
    # post-match filtered data. The whitening array is #
    # computed by averaging the envelipes of many      #
    # pulses and computing the normalized inverse.     #
    # ################################################ #
    
    '''
    pulse_model=mean(range_time(data_file,pulses=1000,normalized=False,beam=1),axis=1)
    whiten=(min(pulse_model[1000:]))/pulse_model
    data_file.rewind()
    data_file.seekmark()
    return whiten

def calibrate(data_file):
    '''
    # ########################## calibrate() ####################### #
    # Given a data file objects, it will return the arbitrary power  #
    # to watts coversion factor. It does so by taking the average of #
    # the square of the noise power calibration at the end of each   #
    # pulse and dividing the actual calibration power by it.         #
    # ############################################################## #
    '''
    return calibration_noise_power/mean(data_file.buf[receive_length-calibration_noise_length:receive_length]**2)

def threshold_detect(file,outfile='out.txt',beam=1,thresh=50.):
    '''
    # ########################### threshold_detect() ################################ #
    # This takes in a file name and returns a tuple of a list of dictionaries of all  #
    # of the post whitening threshold crossings in that file, and the number of       #
    # objects detected. Each dictionary contains the 'Pulse', 'Range', 'Power', 'RCS',#
    # 'Size', and 'Object' number of a particular threshold crossing. It also prints  #
    # to the screen and a text file the "detections."                                 #
    # ############################################################################### #
    '''
    
    outfile_object = open(outfile,'w')
    df=rdvme.rdvme(file) # Creates the file object
    df.seekmark() # sets the pointer to the first file header

    detections = [] # Initializes an empty list of detections
    number_of_detections=0
    number_of_objects=0
    previous_detection_pulse=1
    pulse=1
    calibration_noise_power = 6.624*10**-16 # watts


    print 'File Name: %s ' % df.filestring
    print 'Beam: %s ' % beam
    outfile_object.write('File Name: %s \n' % df.filestring)
    outfile_object.write('Beam: %s \n' % beam)
    outfile_object.write('Pulse    Range[m]    Power[W]   RCS[1/m^2]    Size[m]    Object # \n')

    # This part will create a pulse model for data whitening
    # This is done once for a file
    whiten=whitening_model(df,beam)

    while df.rdvmerec() and size(df.buf) == 2*receive_length:
        # Here I dtermine the absolute power calibration
        # This is done every pulse
        calibration=calibrate(df)
        
        pulse_power=cor(df.buf,beam)**2*calibration        # This is actual pulse power
        whitened_pulse_power=whiten*pulse_power            # This is power above the noise floor
        threshold=thresh*mean(whitened_pulse_power[2000:]) # Threshold is set as some factor above the mean of the whitened noise floor
        
        if (whitened_pulse_power[1000:] > threshold).any():
            # If any element of the whitened power is greater than the threshold
            # Declare a detection and compute the values
            number_of_detections+=1
            detected_sample=argmax(whitened_pulse_power[1000:])+1000
            detected_range=samp2range(detected_sample)
            power=pulse_power[detected_sample]
            radar_cross_section=rcs.pwr2rcs(power,detected_range)
            particle_diameter=rcs.rcs2size(radar_cross_section)

            # This is statement checks to see if the pulses are
            # sequential, assigning the same object number if so. 
            if pulse != previous_detection_pulse+1: number_of_objects+=1
            previous_detection_pulse=pulse

            print 'Pulse: %s    Range: %s    Power: %s    RCS: %s    Size: %s    Object #: %s ' % (str(pulse),str(detected_range),str(power),str(radar_cross_section),str(particle_diameter),str(number_of_objects))
            outfile_object.write('%s    %s    %s    %s    %s    %s \n' % (str(pulse),str(detected_range),str(power),str(radar_cross_section),str(particle_diameter),str(number_of_objects)))
            detections.append({'Pulse' : pulse, 'Range' : detected_range, 'Power' : power, 'RCS' : radar_cross_section, 'Size' : particle_diameter, 'Object' : number_of_objects})
        pulse+=1
        
    print 'Number of Detections: %s ' % str(number_of_detections)
    print 'Number of Objects Detected: %s ' %str(number_of_objects)
    outfile_object.write('Number of Detections: %s \n' % str(number_of_detections))
    outfile_object.write('Number of Objects Detected: %s \n' %str(number_of_objects))

    return detections, number_of_objects

def filter_by_num_pulses(detections, number_of_objects, minimum_pulses=3):
    '''
    # ################ filter_by_num_pulses() ################## #
    # This takes the list of dictionaries from threshold detect  #
    # and the number of objects and returns a tuple of a list of #
    # dictionaries with the detections that had less than        #
    # mimimum_pulses removed, and a revised number of objects    #
    # ########################################################## #
    '''
    number_of_pulses=zeros(number_of_objects)
    for pulse in arange(len(detections)):
        number_of_pulses[detections[pulse]['Object']-1]+=1
    good_detections=0
    good_pulses=0
    for obj in arange(number_of_objects):
        if number_of_pulses[obj] >= minimum_pulses:
            good_pulses+=int(number_of_pulses[obj])
            good_detections+=1
        else:
            del detections[good_pulses:int(number_of_pulses[obj])+good_pulses]
    return detections, good_detections
    

######################################################################
#  From this point on is some older detection code based on FFTs
#  
######################################################################

def detect(file,threshold,outfile='out.txt'):
    
    outfile_object = open(outfile,'w')
    df=rdvme.rdvme(file)
    df.seekmark()

    range_gates=76/2
    pulse=1
    single_pulse=True
    number_of_detections=0

    outfile_object.write('File Name: %s \n' % file)
    
    
    while df.rdvmerec():
        if size(df.buf) < 2*10960: break
        data=waterfall(df.buf)
        for gate in arange(10,range_gates,1):
            doppler=vdop(argmax((data[:,gate:gate+1])),2192*2+1)
            if max((data[:,gate:gate+1]) > threshold) and single_pulse and abs(doppler) < 200. :
                single_pulse=False
                number_of_detections+=1
                range_detect=(gate+.5)*137*baud_rate*c/2
                
                power=max(data[:,gate:gate+1])
                outfile_object.write('Pulse: %s   Range: %s   Vdop: %s   Power: %s \n' % (str(pulse),str(range_detect),str(doppler),str(power)))
                print 'Pulse: %s   Range: %s   Vdop: %s   Power: %s \n' % (str(pulse),str(range_detect),str(doppler),str(power))
        single_pulse=True
        #df.jumpn(1)
        pulse+=1
    outfile_object.write('Total Detections: %s \n' % str(number_of_detections))
    outfile_object.close()
    df.rewind()
    df.seekmark()
    df.rdvmerec()    

def find_detection(file,threshold, beam=1):


    df=file

    fft_len=2192*2+1
    range_gates=76/2
    pulse=1
    single_pulse=True

    if beam==1:
        while df.rdvmerec():
            if size(df.buf) < 2*10960: break

            data=waterfall(df.buf)

            for gate in arange(10,range_gates,1):
                doppler=vdop(argmax((data[:,gate:gate+1])),fft_len)

                if max((data[:,gate:gate+1]) > threshold) and single_pulse and abs(doppler) < 200. :
                    range_detect=((gate)*137+.5*fft_len)*baud_rate*c/2
                    power=max(data[:,gate:gate+1])

                    print 'Pulse: %s   Range: %s   Vdop: %s   Power: %s ' % (str(pulse),str(range_detect),str(doppler),str(power))

                    return df

            pulse+=1

    elif beam==2:
        while df.rdvmerec():
            if size(df.buf) < 2*10960: break

            data=waterfall(df.buf[10960:])

            for gate in arange(10,range_gates,1):
                doppler=vdop(argmax((data[:,gate:gate+1])),fft_len)

                if max((data[:,gate:gate+1]) > threshold) and single_pulse and abs(doppler) < 200. :
                    range_detect=((gate)*137+.5*fft_len)*baud_rate*c/2
                    power=max(data[:,gate:gate+1])

                    print 'Pulse: %s   Range: %s   Vdop: %s   Power: %s ' % (str(pulse),str(range_detect),str(doppler),str(power))

                    return df

            pulse+=1
            
    print 'No pulses found...'



if __name__ == "__main__":
    print 'Hello World'
    
    input_file=sys.argv[1]

    threshold_detect(input_file)
