import numpy as np

Pt=10.**6
Gt=10.**7.3
Gr=10.**4.72
lam=.126
tau=.003
Rt=500000.
Rr=500000.
k_B=1.38*10**-23
Tsys=107
Loss=1
SNR_min=2



def SNR(d,Pt=Pt,Gt=Gt,Gr=Gr,Rt=Rt,Rr=Rr,Tsys=Tsys,Loss=Loss,lam=lam,tau=tau):
    S=0.111*np.pi**2*Pt*Gt*Gr*tau/(4*lam**2*Rt**2*Rr**2*k_B*Tsys*Loss)*d**6
    return S

def diameter(Loss,Pt=Pt,Gt=Gt,Gr=Gr,Rt=Rt,Rr=Rr,Tsys=Tsys,lam=lam,tau=tau,SNR_min=SNR_min):
    d=(0.111*np.pi**2*Pt*Gt*Gr*tau/(4*lam**2*Rt**2*Rr**2*k_B*Tsys*SNR_min*Loss))**(-1./6)
    return d

def rcs(Loss,Pt=Pt,Gt=Gt,Gr=Gr,Rt=Rt,Rr=Rr,Tsys=Tsys,lam=lam,tau=tau,SNR_min=SNR_min):
    rcs=(4*np.pi)**3*Rt**2*Rr**2*k_B*Tsys/(Pt*Gt*Gr*lam**2*tau)*SNR_min
    return rcs

def pwr2rcs(Pr,R,Pt=500000.,G=10**5.89,lam=.7, compression_gain=220.):
    return (4*np.pi)**3*R**4*Pr/((G)**2*lam**2*Pt*compression_gain)

def rcs2size(rcs,lam=.7):
    diameter=(4*rcs*lam**4/(9*np.pi**5))**(1./6)
    return diameter
