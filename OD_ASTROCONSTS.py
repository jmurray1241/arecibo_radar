'''
#-----------------------------------------------------------
#  FILENAME: astroconstants.dat
#  AUTHOR:   Drew Vavrin
#  DESC:     This file consists of astrodynamical constants
#            that are useful for scientific programming.
#            To add a new constant, use the following 
#            strict format:
#               variable<TAB>description<TAB>value<TAB>reference
#  NOTES:    All information related to each constant has 
#            been verified by the Orbital Debris Modeling
#            Group at JSC.  This includes the following:
#              - spelling of the constant variable name
#              - description of the constant
#              - numerical value of the constant
#              - reference where the value was found
#-----------------------------------------------------------

#***********************************************************************************
# REFERENCES:  

% {A}	IAU Division I Working Group, Numerical Standards for Fundamental Astronomy, IAU 2009 System of Astronomical Constants, [:CRLF:] http://maia.usno.navy.mil/NSFA/IAU_2009consts.html.
% {B}	D. A. Vallado and W. D. McClain, Fundamentals of Astrodynamics and Applications (ISBN 0-07-066834-5). [:CRLF:] JGM-2 Constants and Conversions; Appendix D.2 Planetary Constants; Table D-3 and Table D-4, pg 877-878.

#***********************************************************************************


# The header below will be copied to all generated header files.
#    see generate.pl for more details

@==================================================================
@
@  NOTES:
@
@  1) Mmoon calculated using Mass Ratio of Moon-Earth
@     Mmoon = (mass ratio of Moon-Earth) * (Me), and
@     GMmoon calculated using Mmoon
@
@==================================================================


au_km   	astronomical unit [km]	{A}	
au_mks  	astronomical unit [m]	{A}	
au_cgs  	astronomical unit [cm]	{A}	
c_km    	speed of light in a vacuum [km/s]	{A}	
c_mks   	speed of light in a vacuum [m/s]	{A}	
c_cgs   	speed of light in a vacuum [cm/s]	{A}	
G_mks   	universal gravitational constant [m^3/(kg * s^2)]	{A}	
G_cgs   	universal gravitational constant [cm^3/(g * s^2)]	{A}	
rE_km   	equatorial radius of Earth [km]	{A}	
rE_mks  	equatorial radius of Earth [m]	{A}	
rE_cgs  	equatorial radius of Earth [cm]	{A}	
rEPol_km   	polar radius of Earth [km]	{B}	
rEPol_mks  	polar radius of Earth [m]	{B}	
rEPol_cgs  	polar radius of Earth [cm]	{B}	
Me_mks  	Mass of Earth [kg]	{A}	
Me_cgs  	Mass of Earth [g]	{A}	
GMe     	geocentric gravitational constant [km^3/s^2]	{A}	
GMe_mks 	geocentric gravitational constant [m^3/s^2]	{A}	
GMe_cgs 	geocentric gravitational constant [cm^3/s^2]	{A}	
aE_km   	semi-major axis of heliocentric Earth orbit (Earth-Sun distance,~1 AU) [km]	{B}	
aE_mks  	semi-major axis of heliocentric Earth orbit (Earth-Sun distance,~1 AU) [m]	{B}	
aE_cgs  	semi-major axis of heliocentric Earth orbit (Earth-Sun distance,~1 AU) [cm]	{B}	
_J2     	Earth J2-term [dimensionless, no units]	{A}	
_J3     	Earth J3-term [dimensionless, no units]	{B}	
_J4     	Earth J4-term [dimensionless, no units]	{B}	
Msun_mks	Mass of the Sun [kg]	{A}	
Msun_cgs	Mass of the Sun [g]	{A}	
GMsun   	heliocentric gravitational constant [km^3/s^2]	{A}	
GMsun_mks	heliocentric gravitational constant [m^3/s^2]	{A}	
GMsun_cgs	heliocentric gravitational constant [cm^3/s^2]	{A}	
Mmoon2Me	Mass Ratio of Moon-Earth [dimensionless, no units]	{A}	
Mmoon_mks	Mass of the Moon [kg]	{A}	
Mmoon_cgs	Mass of the Moon [g]	{A}	
GMmoon  	Moon gravitational constant [km^3/s^2]	{A}	
GMmoon_mks	Moon gravitational constant [m^3/s^2]	{A}	
GMmoon_cgs	Moon gravitational constant [cm^3/s^2]	{A}	
aM_km   	semi-major axis of geocentric lunar orbit (Earth-Moon distance) [km]	{B}	
aM_mks  	semi-major axis of geocentric lunar orbit (Earth-Moon distance) [m]	{B}	
aM_cgs  	semi-major axis of geocentric lunar orbit (Earth-Moon distance) [cm]	{B}	
E_RotRate	Nominal Mean Angular Velocity of the Earth [rad/s]	{A}	
E_oblq  	Mean Obliquity of the ecliptic (Earth) [rad]	{A}	
E_ecc   	Earth Eccentricity [dimensionless, no units]	{B}	
E_flat   	Earth Flattening Constant [dimensionless, no units]	{A}	
E_EccSqrd	Eccentricity of the Earth ellipsoid squared [dimensionless, no units]	{A}	
'''

def au_km   (): 
 	 return 1.49597870700e08
def au_mks  (): 
 	 return 1.49597870700e11
def au_cgs  (): 
 	 return 1.49597870700e13
def c_km    (): 
 	 return 2.99792458e05
def c_mks   (): 
 	 return 2.99792458e08
def c_cgs   (): 
 	 return 2.99792458e10
def G_mks   (): 
 	 return 6.67428e-11
def G_cgs   (): 
 	 return 6.67428e-08
def rE_km   (): 
 	 return 6.3781366e03
def rE_mks  (): 
 	 return 6.3781366e06
def rE_cgs  (): 
 	 return 6.3781366e08
def rEPol_km   (): 
 	 return 6.35675160050e03
def rEPol_mks  (): 
 	 return 6.35675160050e06
def rEPol_cgs  (): 
 	 return 6.35675160050e08
def Me_mks  (): 
 	 return 5.9722e24
def Me_cgs  (): 
 	 return 5.9722e27
def GMe     (): 
 	 return 3.986004418e05
def GMe_mks (): 
 	 return 3.986004418e14
def GMe_cgs (): 
 	 return 3.986004418e20
def aE_km   (): 
 	 return 1.49598023e08
def aE_mks  (): 
 	 return 1.49598023e11
def aE_cgs  (): 
 	 return 1.49598023e13
def _J2     (): 
 	 return 1.082635e-03
def _J3     (): 
 	 return -2.54000e-06
def _J4     (): 
 	 return -1.58000e-06
def Msun_mks(): 
 	 return 1.9884e30
def Msun_cgs(): 
 	 return 1.9884e33
def GMsun   (): 
 	 return 1.32712442099e11
def GMsun_mks(): 
 	 return 1.32712442099e20
def GMsun_cgs(): 
 	 return 1.32712442099e26
def Mmoon2Me(): 
 	 return 1.23000371e-02
def Mmoon_mks(): 
 	 return 7.34583e22
def Mmoon_cgs(): 
 	 return 7.34583e25
def GMmoon  (): 
 	 return 4.90281e03
def GMmoon_mks(): 
 	 return 4.90281e12
def GMmoon_cgs(): 
 	 return 4.90281e18
def aM_km   (): 
 	 return 3.84400e05
def aM_mks  (): 
 	 return 3.84400e08
def aM_cgs  (): 
 	 return 3.84400e10
def E_RotRate(): 
 	 return 7.292115e-05
def E_oblq  (): 
 	 return 4.090877234e-01
def E_ecc   (): 
 	 return 1.6708617e-02
def E_flat   (): 
 	 return 3.35281969789619e-02
def E_EccSqrd(): 
 	 return 6.69439799586579e-03
