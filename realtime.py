import numpy as np
import matplotlib.pyplot as plt

plt.axis([0, 10, 0, 1])
plt.ion()

for i in range(1000):
    y = np.random.random()
    start = (i - 50)
    stop = i 
    plt.xlim(start,stop)
    plt.scatter(i, y)
    plt.pause(0.01)

while True:
    plt.pause(0.01)
