#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 16:50:28 2019

@author: jamesmurray
"""

class Measurer:
    def __init__(self):
        MAG_BIAS = 0.000
        WEIGHTING_ID =      0
        MEAS_InterpFFT = true
        MEAS_FFT_SIZE =  16384
        LEGACY_MONOPULSE = false
        MF_LEAST_SQUARES = false
        USE_INTEGRATED_RANGE = true
        USE_PRINCIPAL_FOR_GROUP = false
        USE_PRINCIPAL_RANGE = false
        USE_PRINCIPAL_DOPPLER = false
        MEAS_RANGE_WIN =      3
        MEAS_DOPP__WIN =      3