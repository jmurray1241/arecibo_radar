# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 07:55:29 2019

@author: jimurray
"""

import numpy as np
from scipy.ndimage.filters import uniform_filter1d

baud_rate = 1/2.*10**-6      # [s]   This is the time between samples
sample_rate = 1/baud_rate # [samples/s] 
c = 3.*10**8              # [m/s] Speed of light
f_0= 430.*10**6           # [Hz]  Center Frequency of the Radar
start_of_pulse = 7        # [samples] sample at which the pulse begins
pulse_length = 880        # [samples]
PRI_length = 10960    # [samples]
calibration_noise_power = 6.624*10**-16 # [W] 
start_of_noise = 9361 # [samples]
end_of_noise = 10177  # [samples]
blank_length = 1250 # [samples] length of time for which the receiver is covered
channels_per_buf = 2

class Observation:
    
    def __init__(self, df, PRI, beam = 1):
        try:
            df.buf
        except:
            df.seekmark()
            df.rdvmerec()
            
        # From the df object
        self.Time = df.time
        self.PRI = PRI        
        self.Detection_Channel = beam
        self.Az_Enc = df.az - 180.
        self.El_enc = 90. - df.zag
        # From the Detector
        self.SNR_PP = np.nan
        self.SNR_OP = np.nan
        self.Valid_Flag = False
        self.Range_Sample_Offset = np.nan
        self.Doppler_Estimate = np.nan
        

        # Assigned when added to a Event
        self.Event = np.nan
        # From the Measurer
        self.Range = np.nan
        self.Doppler = np.nan
        #self.Altitude
        self.RCS_PP = np.nan
        self.RCS_OP = np.nan
        self.RCS_TOT = np.nan
        self._parse_buf(df)
    
#    def detect(self, Detector):
#        self._parse_df()
        
    
    def _parse_buf(self, df, span=1000, filt='mean'):
        
        self.pulse = []
        self.receive = []
        self.calibration_const = []
        self.noise_floor = []
        
        for channel in range(channels_per_buf):
            if channel == 0:
                buffer_offset = 0
            if channel == 1:
                buffer_offset = PRI_length
            self.pulse.append(df.buf[start_of_pulse+buffer_offset : start_of_pulse+pulse_length+buffer_offset])
            self.receive.append(df.buf[blank_length+buffer_offset: PRI_length+buffer_offset])
            self.calibration_const.append(calibration_noise_power/np.mean(df.buf[start_of_noise+buffer_offset: end_of_noise+buffer_offset]**2))
            self.noise_floor.append(self._get_noise(channel, filt))

        return 1
        

    def _get_noise(self, channel, filt, span = 1000, normalized = False):
        if filt == 'mean':
            return uniform_filter1d(self.receive[channel]**2, size=span)
        elif filt == 'median':
            filt_buf = np.zeros(len(self.receive[channel]))
            for i in range(len(self.receive[channel])):
                filt_buf[i] = np.median(self.receive[channel][i-span/2:i+span/2]**2)
            if normalized:
                return filt_buf/np.max(filt_buf)
            return filt_buf
    







#angle=[]
#timestamp=[]
#baudlen=[]
#infostr=[]
#rf=[]
#bitspersam=[]
#ipp=[]
#rihdr=[]
#buf=[]
#ristr=[]
#codetype=[]
#nnsam1=[]
#date=[]
#nnsam2=[]
#nsam1=[]
#nsam2=[]
#gd=[]
#nsamwin=[]
#stdstr=[]
#gd2=[]
#gw=[]
#txsams=[]
#zac=[]
#hdrlen=[]
#reclen=[]
#zag=[]
#
#
#
#while df.rdvmerec():
#    angle.append(df.az)
#    timestamp.append(df.time)
#    baudlen.append(df.baudlen)
#    infostr.append(df.infostr)
#    rf.append(df.rf)
#    bitspersam.append(df.bitspersam)
#    ipp.append(df.ipp)
#    rihdr.append(df.rihdr)
#    buf.append(df.buf)
#    ristr.append(df.ristr)
#    codetype.append(df.codetype)
#    nnsam1.append(df.nnsam1)
#    date.append(df.date)
#    nnsam2.append(df.nnsam2)
#    nsam1.append(df.nsam1)
#    nsam2.append(df.nsam2)
#    gd.append(df.gd)
#    nsamwin.append(df.nsamwin)
#    stdstr.append(df.stdstr)
#    gd2.append(df.gd2)
#    gw.append(df.gw)
#    txsams.append(df.txsams)
#    zac.append(df.zac)
#    hdrlen.append(df.hdrlen)
#    reclen.append(df.reclen)
#    zag.append(df.zag)






