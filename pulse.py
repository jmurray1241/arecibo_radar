# -*- coding: utf-8 -*-
import arecibo2 as ao
import rdvme
import numpy as np
from struct import *
import sys
from scipy.signal import medfilt
from scipy.ndimage.filters import uniform_filter1d
from time import time
import config

baud_rate = 1/2.*10**-6      # [s]   This is the time between samples
sample_rate = 1/baud_rate # [samples/s] 
c = 3.*10**8              # [m/s] Speed of light
f_0= 430.*10**6           # [Hz]  Center Frequency of the Radar
start_of_pulse = 7        # [samples] sample at which the pulse begins
pulse_length = 880        # [samples]
receive_length = 10960    # [samples]
calibration_noise_power = 6.624*10**-16 # [W] 
start_of_noise = 9361 # [samples]
end_of_noise = 10177  # [samples]
blank_length = 1250 # [samples] length of time for which the receiver is covered

class pulse(rdvme.rdvme):
    ''

    ''
    def __init__(self, filestring):
        self.f = open(filestring, 'rb')
        self.filestring = filestring
        self.pulse_number = 0
        self.seekmark()
    
    def rdvmerec(self, beam=1, span=1000, filt='mean'):
        total_start = time()
        loc = self.f.tell()
        self.infostr = self.f.read(12)
        if len(self.infostr) == 0: return 0
        self.hdrinfo = unpack_from('>4sii', self.infostr)
        self.f.seek(loc, 0)
        self.hstr = self.f.read(self.hdrinfo[1])
        if len(self.hstr) == 0: return 0
        self.stdstr = '>4s2i8s7i16B16i'
        self.stdhdr = unpack_from(self.stdstr, self.hstr)
        self.ptype = self.stdhdr[3]
        self.ristr =  '>' + str(self.stdhdr[12]*16) + 'x' + str(self.stdhdr[14]//4) + 'i'
        self.prstr =  '>' + str(self.stdhdr[20]*16) + 'x' + str(self.stdhdr[22]//4) + 'i'
        self.spsstr = '>' + str(self.stdhdr[24]*16 + self.stdhdr[25]//16) + 'x8s7fi21f3sxi3f2if2if2i'
        self.rihdr =  unpack_from(self.ristr, self.hstr)
        if self.stdhdr[22] > 0: 
            self.prhdr = unpack_from(self.prstr, self.hstr)
        self.spshdr =  unpack_from(self.spsstr, self.hstr)
        if self.stdhdr[22] > 0:
            self.buf = np.fromfile(self.f, dtype=np.float32, count=(self.hdrinfo[2] - self.hdrinfo[1])//4, sep= "").byteswap()
        else:
            self.buf = np.fromfile(self.f, dtype=np.int16, count=(self.hdrinfo[2] - self.hdrinfo[1])//2, sep= "").byteswap().astype(np.float32)
        rdvme.setstdparams(self)
        rdvme.setspsparams(self)
        rdvme.setriparams(self)
        rdvme.setprparams(self)

        param_stop=time()
        #print 'Pulse parameter extraction took %s ms...' % str(1000*(param_stop-total_start))

        
        if beam == 1:
            separation_start = time()
            self.pulse = self.buf[start_of_pulse : start_of_pulse+pulse_length]
            #self.receive = np.append(np.zeros(blank_length), self.buf[blank_length : receive_length])
            self.receive = (self.buf[blank_length : receive_length])
            separation_stop = time()
            #print u'Pulse separation took %s µs...' % str((separation_stop-separation_start)*10**6)
            cal_start = time()
            self.calibration_const = calibration_noise_power/np.mean(self.buf[start_of_noise : end_of_noise]**2)
            cal_stop = time()
            #print 'Calibration constant calculation took %s ms...' %str(1000*(cal_stop-cal_start))
            #self.noise_floor = medfilt(self.receive**2,kernel_size=999)
            med_start = time()
            if filt == 'median':
                self.noise_floor = self.median_filter()
            elif filt == 'mean':
                self.noise_floor = self.get_noise()
            med_stop = time()
            #print "Noise Floor took %s ms..." % str(1000*(med_stop-med_start))
        elif beam == 2:
            separation_start = time()
            self.pulse = self.buf[start_of_pulse+receive_length : start_of_pulse+pulse_length+receive_length]
            #self.receive = np.append(np.zeros(blank_length), self.buf[blank_length : receive_length])
            self.receive = self.buf[blank_length+receive_length : 2*receive_length]
            separation_stop = time()
            #print u'Pulse separation took %s µs...' % str((separation_stop-separation_start)*10**6)
            cal_start = time()
            self.calibration_const = calibration_noise_power/np.mean(self.buf[start_of_noise+receive_length: end_of_noise+receive_length]**2)
            cal_stop = time()
            #print 'Calibration constant calculation took %s ms...' %str(1000*(cal_stop-cal_start))
            #self.noise_floor = medfilt(self.receive**2,kernel_size=999)
            med_start = time()
            if filt == 'median':
                self.noise_floor = self.median_filter()
            elif filt == 'mean':
                self.noise_floor = self.get_noise()
            med_stop = time()
            #print "Noise Floor took %s ms..." % str(1000*(med_stop-med_start))

            #self.pulse = self.buf[start_of_pulse+receive_length : start_of_pulse+pulse_length+receive_length]
            #self.receive = self.buf[blank_length+receive_length : 2*receive_length]
            #self.calibration_const = calibration_noise_power/np.mean(self.buf[start_of_noise+receive_length: end_of_noise+receive_length]**2)
            #self.noise_floor = self.median_filter()

        self.pulse_number = self.pulse_number+1

        total_stop = time()
        #print 'Pulse initialization took %s ms...' % str(1000*(total_stop-total_start))
        
        return 1
    #  method to advance n records
    def jumpn(self, nad):
        self.pulse_number = self.pulse_number + nad - 1
        n = nad
        while nad > 0:
            loc = self.f.tell()
            hdstr = self.f.read(12)
            self.hdrinfo = unpack_from('>4sii', hdstr)
            self.f.seek(self.hdrinfo[2] - 12, 1)
            nad -= 1

    # method to go back to beginning of the file
    def rewind(self):
        self.f.seek(0, 0)
        self.seekmark()
        self.pulse_number = 0
        
    def median_filter(self, span = 1000, normalized = False):
        filt_buf = np.zeros(len(self.receive))
        for i in range(len(self.receive)):
            filt_buf[i] = np.median(self.receive[i-span/2:i+span/2]**2)
        if normalized:
            return filt_buf/np.max(filt_buf)
        return filt_buf

    def get_noise(self, span = 1000):
        return uniform_filter1d(self.receive**2, size=span) 

    def mean_filter(self, span = 1000, normalized = False):
        filt_buf = np.zeros(len(self.receive))
        for i in range(len(self.receive)):
            filt_buf[i] = np.mean(self.receive[i-span/2:i+span/2]**2)
        if normalized:
            return filt_buf/np.max(filt_buf)
        return filt_buf
    
    def moving_average(self, span=1000) :
        ret = np.cumsum(self.receive**2, dtype=float)
        ret[span:] = ret[span:] - ret[:-span]
        return ret[span - 1:] / span
    
    def matched_filter(self):
        return np.correlate(self.receive/np.sqrt(self.noise_floor), self.pulse, mode='same')/len(self.receive)

    def threshold_detect(self, prob_false_alarm=10.**-2):
        #variance = np.std(self.receive[500:960]/np.sqrt(self.noise_floor)[500:960])**2
        #print variance
        #variance = np.sqrt(2)
        threshold = np.sqrt(2)*np.log(1/prob_false_alarm)
        #threshold = 8.
        #print "Threshold: %s " % str(threshold)
        matched = abs(self.matched_filter())
        if (matched > threshold).any():
            #print 'Yay!'
            #print "Detection at %s km!" % str((np.nanargmax(matched)+blank_length-pulse_length/2)*baud_rate*c/2/1000)
            return 1
        else:
            #print 'Sorry...'
            return 0

class tracker:

    def __init__(self, file_name, beam=1):
        self.file_name = file_name
        self.beam = beam
        self.pulse = []
        self.slant_range = []
        self.object_ID = []
        self.cal_const = []
        self.rcs = []
        self.size = []
        self.range_rate = []
        self.number_of_objects = 0
        self.number_of_pulses = 0
        self.find_detections()


    def find_detections(self, filt='mean'):
    
        df=pulse(config.path + self.file_name)

        previous_detection_pulse = 1

        while df.rdvmerec(beam=self.beam, filt=filt) and len(df.buf) == 2*receive_length:
            if df.threshold_detect():
                
                if df.pulse_number != previous_detection_pulse+1: self.number_of_objects += 1
                previous_detection_pulse = df.pulse_number

                self.pulse.append(df.pulse_number)
                self.slant_range.append(sam2range(np.nanargmax(df.matched_filter())))
                self.object_ID.append(self.number_of_objects)
                self.cal_const.append(df.calibration_const)

        self.number_of_pulses = len(self.pulse)
                
        return 1
    
    #def get_doppler()

    def pulse_filter(self, minimum_pulses=3):

        if self.number_of_pulses < 1: return 0
        
        num_of_pulses = np.zeros(self.number_of_objects)
        number_of_objects_deleted = 0
        
        for i in range(self.number_of_pulses):
            num_of_pulses[self.object_ID[i]-1]+=1
        good_detections=0
        good_pulses=0
        for obj in range(self.number_of_objects):
            if num_of_pulses[obj] >= minimum_pulses:
                good_pulses += int(num_of_pulses[obj])
                good_detections+=1
            else:
                del self.pulse[good_pulses:int(num_of_pulses[obj])+good_pulses]
                del self.slant_range[good_pulses:int(num_of_pulses[obj])+good_pulses]
                del self.object_ID[good_pulses:int(num_of_pulses[obj])+good_pulses]
                number_of_objects_deleted += 1

        self.number_of_objects = self.number_of_objects - number_of_objects_deleted
        self.number_of_pulses = len(self.pulse)

        return 1

    def range_filter(self, minimum_pulses=3):

        if self.number_of_pulses < 1: return 0
        
        to_del = []
        for i in range(self.number_of_pulses):
            #print i
            ID = self.object_ID[i]
            ID_indicies = (np.where(np.array(self.object_ID) == ID)[0])


            range_of_slant_ranges = np.max(np.array(self.slant_range)[ID_indicies])-np.min(np.array(self.slant_range)[ID_indicies])

            if range_of_slant_ranges > 500.:#500 meters
                for index in ID_indicies:
                    to_del.append(index)
        #return to_del
        to_del = sorted(list(set(to_del)),reverse=True)
        #to_del = np.unique(to_del)
            
        for i in to_del:
            del self.pulse[i]
            del self.slant_range[i]
            del self.object_ID[i]

        self.number_of_pulses = len(self.pulse)
        self.number_of_objects = len(set(self.object_ID))
        print self.number_of_pulses
        print self.number_of_objects
        
        return 1
            

    def save_detections(self, directory_out = config.path):
        file_out = directory_out + 'tracks_' + self.file_name + '.txt'
        num_detections = len(self.pulse)
        #data = np.vstack((range_samples, detections))
        np.savetxt(file_out, np.column_stack([self.pulse,self.slant_range, self.object_ID]), delimiter='    ', header='File: '+self.file_name+'\nBeam: '+str(self.beam)+'\nPulse  Range[m]  Object_ID', footer= str(num_detections)+' returns exceeded threshold\n' + str(self.number_of_objects) + ' objects were identified', fmt='%u')
        return 1



###########################
        

        
def find_detections(file_name, beam=1, filt='mean'):

    detections   = []
    range_sample = []
    
    df=pulse(config.path + file_name)

    number_of_objects = 0
    previous_detection_pulse = 1
    
    while df.rdvmerec(beam=beam, filt=filt) and len(df.buf) == 2*receive_length:
        if df.threshold_detect():
            detections.append(df.pulse_number)
            range_sample.append(np.nanargmax(df.matched_filter()))
            if df.pulse_number != previous_detection_pulse+1: number_of_objects += 1
            previous_detection_pulse = df.pulse_number

    range_sample = np.array(range_sample)
    detections = np.array(detections)
    return range_sample, detections, number_of_objects

def filter_by_pulses(range_samples, detections, number_of_objects, minimum_pulses=3):
    data = np.array((detections,range_samples)).T
    
    number_of_pulses = np.zeros(number_of_objects)
    print len(detections)
    print len(number_of_pulses)
    for i in range(len(detections)):
        number_of_pulses[detections[i]-1] += 1
    good_detections=0
    good_pulses=0
    for obj in arange(number_of_objects):
        if number_of_pulses[obj] >= minimum_pulses:
            good_pulses+=int(number_of_pulses[obj])
            good_detections+=1
        else:
            data = np.delete(data,range(good_pulses,int(number_of_pulses[obj])+good_pulses),0)

    range_samples = data.T[1]
    detections = data.T[0]
                    
    return range_samples, detections, good_detections    
    
def save_detections(file_name, detections, range_samples, beam=1):
    file_out = config.path + 'detect_' + file_name + '.txt'
    num_detections = len(detections)
    #data = np.vstack((range_samples, detections))
    np.savetxt(file_out, np.column_stack([detections,range_samples]), delimiter='    ', header='File: '+file_name+'\nBeam: '+str(beam)+'\nPulse    Range', footer= str(num_detections)+' returns exceeded threshold', fmt='%u')
    return 1

        
def time_it(N=10,beam=1,filt='mean'):
    detections = []
    df=pulse('/Users/jamesmurray/t1193_20160205.000')  
    start = time()
    for i in range(N):
        #print "Pulse %s: " % str(df.pulse_number)
        df.rdvmerec(beam=beam,filt=filt)
        if df.threshold_detect(): detections.append(df.pulse_number)
        
        
    end = time()
    print "Total time: %s s   Average time: %s s" % (str(end-start),str((end-start)/N))
    return detections



def sam2range(sam):
    '''
    Expects sample number from df.matched_filter()
    '''
    return (sam + blank_length - pulse_length/2) * baud_rate * c/2









def median_filter(buf, span = 1000, normalized = False):
    filt_buf = np.zeros(len(buf))
    for i in range(len(buf)):
        start = i - span/2
        stop =  i + span/2
        if start < 0: start = 0
        if stop > len(buf): stop = len(buf)
        filt_buf[i] = np.median(buf[start:stop])
    if normalized:
        return filt_buf/np.max(filt_buf)
    return filt_buf

def file_beam(file_name, beam=1, start_pulse=0, end_pulse=48555, span=10):
    #path = '/Users/jamesmurray/'
    df = pulse(config.path+file_name)
    power_max = []
    ranges = []

    ## while df.rdvmerec(filt='mean'):
    ##     temp = df.matched_filter()
    ##     the_point = np.nanargmax(temp)
    ##     beam.append(df.noise_floor[the_point]*temp[the_point])
    
    ## while df.rdvmerec(filt='mean'):
    ##     if len(df.buf) < 2*receive_length: return median_filter(beam/median_filter(beam),span=10)
    ##     temp = df.matched_filter()
    ##     the_point = np.nanargmax(temp)
    ##     beam.append(np.max(df.noise_floor*temp))
    if start_pulse > 0:
        df.jumpn(start_pulse)
    
    for i in range(start_pulse, end_pulse):
        df.rdvmerec(filt='mean',beam = beam)
        if (len(df.buf) < 2*receive_length) and span > 0:
            return median_filter(power_max/median_filter(power_max),span=span)
        elif (len(df.buf) < 2*receive_length):
            return power_max/median_filter(power_max)
            
        temp = df.matched_filter()
        the_point = np.nanargmax(df.noise_floor*temp)
        power_max.append((df.noise_floor*temp)[the_point])
        ranges.append(the_point)
        

    
    #beam = np.array(beam)
    #return beam
    if span > 0:
        return median_filter(power_max/median_filter(power_max),span=span)
    else:
        return power_max/median_filter(power_max)
    #return np.where(median_filter(beam/median_filter(beam),span=10))[0]

def write_pulses(file_in, beam = 1):
    file_out = config.path + 'detections_' + file_in + '.txt'
    threshold = 1.26
    detections = np.where(file_beam(file_in, beam=beam)>threshold)[0]
    num_detections = len(detections)
    np.savetxt(file_out, detections, header='File: '+file_in+'\nBeam: '+str(beam)+'\nPulse Number', footer= str(num_detections)+' returns exceeded threshold', fmt='%u')
    return 1


## class meta_data:

##     def __init__(self):
##         self.angle=[]
##         self.timestamp=[]
##         self.baudlen=[]
##         self.infostr=[]
##         self.rf=[]
##         self.bitspersam=[]
##         self.ipp=[]
##         self.rihdr=[]
##         self.buf=[]
##         self.ristr=[]
##         self.codetype=[]
##         self.nnsam1=[]
##         self.date=[]
##         self.nnsam2=[]
##         self.nsam1=[]
##         self.nsam2=[]
##         self.gd=[]
##         self.nsamwin=[]
##         self.stdstr=[]
##         self.gd2=[]
##         self.gw=[]
##         self.txsams=[]
##         self.zac=[]
##         self.hdrlen=[]
##         self.reclen=[]
##         self.zag=[]


## def reset_meta_data():
##         self.angle=[]
##         self.timestamp=[]
##         self.baudlen=[]
##         self.infostr=[]
##         self.rf=[]
##         self.bitspersam=[]
##         self.ipp=[]
##         self.rihdr=[]
##         self.buf=[]
##         self.ristr=[]
##         self.codetype=[]
##         self.nnsam1=[]
##         self.date=[]
##         self.nnsam2=[]
##         self.nsam1=[]
##         self.nsam2=[]
##         self.gd=[]
##         self.nsamwin=[]
##         self.stdstr=[]
##         self.gd2=[]
##         self.gw=[]
##         self.txsams=[]
##         self.zac=[]
##         self.hdrlen=[]
##         self.reclen=[]
##         self.zag=[]

##     def meta_data(number_of_files=1):
##         path = '/Users/jamesmurray/'
##         for file in range(0,number_of_files-1):
##             filename="t1193_20160205.00" + str(file)
##             df=ao.rdvme.rdvme(path + filename)
##             df.seekmark()

##         while df.rdvmerec():
##             angle.append(df.az)
##             timestamp.append(df.time)
##             baudlen.append(df.baudlen)
##             infostr.append(df.infostr)
##             rf.append(df.rf)
##             bitspersam.append(df.bitspersam)
##             ipp.append(df.ipp)
##             rihdr.append(df.rihdr)
##             buf.append(df.buf)
##             ristr.append(df.ristr)
##             codetype.append(df.codetype)
##             nnsam1.append(df.nnsam1)
##             date.append(df.date)
##             nnsam2.append(df.nnsam2)
##             nsam1.append(df.nsam1)
##             nsam2.append(df.nsam2)
##             gd.append(df.gd)
##             nsamwin.append(df.nsamwin)
##             stdstr.append(df.stdstr)
##             gd2.append(df.gd2)
##             gw.append(df.gw)
##             txsams.append(df.txsams)
##             zac.append(df.zac)
##             hdrlen.append(df.hdrlen)
##             reclen.append(df.reclen)
##             zag.append(df.zag)


def time_it(N=10,beam=1,filt='mean'):
    detections = []
    df=pulse('/Users/jamesmurray/t1193_20160205.000')  
    start = time()
    for i in range(N):
        #print "Pulse %s: " % str(df.pulse_number)
        df.rdvmerec(beam=beam,filt=filt)
        if df.threshold_detect(): detections.append(df.pulse_number)
        
        
    end = time()
    print "Total time: %s s   Average time: %s s" % (str(end-start),str((end-start)/N))
    return detections





def one_bit(sig):
    signal=copy(sig)
    for i in range(len(signal)):
        if signal[i]>=0: signal[i]=1
        if signal[i]<0:  signal[i]=-1
    return signal

## noise_m=zeros((1000,1024))
## variances=linspace(.0001,20,100)
## snr_gain=[]
## snr_m=zeros((100,10))
## for trial in range(10):
##     snr_gain=[]
##     for variance in variances:
##         for i in range(1024):
##             noise_m[:,i]=one_bit(random.normal(scale=variance,size=1000)+sin(arange(1000)+random.normal()))
##         blah=mean(noise_m,axis=1)
##         sig=random.normal(scale=variance,size=1000)+sin(arange(1000))

##         sig_fft=(10*log10(abs(fft.rfft(sig))**2))
##         blah_fft=(10*log10(abs(fft.rfft(blah))**2))

##         sig_snr=max(sig_fft)-mean(sig_fft[100:250])
##         blah_snr=max(blah_fft)-mean(blah_fft[100:250])

##         snr_gain.append(blah_snr-sig_snr)

##     snr_gain=array(snr_gain)
##     snr_m[:,trial]=snr_gain



