# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 07:51:24 2019

@author: jimurray
"""

import numpy as np
import matplotlib.pyplot as plt

baud_rate = 1/2.*10**-6   # [s]   This is the time between samples
sample_rate = 1/baud_rate # [samples/s] 
c = 3.*10**8              # [m/s] Speed of light
f_0= 430.*10**6           # [Hz]  Center Frequency of the Radar
start_of_pulse = 7        # [samples] sample at which the pulse begins
pulse_length = 880        # [samples]
PRI_length = 10960        # [samples]
blank_length = 1250       # [samples] length of time for which the receiver is covered


class Detector:
    
    def __init__(self):
        # Detector Parameters
        self.Detector_Channel = 0
        self.N = 16
        self.M = 5
        self.Detection_Threshold = 5.650
        self.Detector_InterpFFT = False
        self.SNR_Test = 1
        self.Valid_SNR = 5.650
        self.Doppler_Test = 1
        self.Valid_Doppler_Win = 8.000
        self.Range_Test = 1
        self.Valid_Range_Win = 0.000
        self.Count_Test = 1
        self.Valid_Count = 3
        self.FFT_SIZE = 880
        self.N_OBS = 880
        self.N_SKIP = 1
        # 
        SNR_PP = np.nan
        SNR_OP = np.nan
        Valid_Flag = False
        Range_Sample_Offset = np.nan
        Doppler_Estimate = np.nan
    
    def detect(self, observation):
        RDI = self.RDI(observation)
        
    
    def RDI(self, observation):
        '''
        # ############# rangeVrange_rate() ##################### #
        # Pass this function a data buffer and it will return a  #
        # range v. range-rate plot where each vertical slice is  #
        # an fft of length fft_len zero padded to a total length #
        # of 8 times fft_len. Each fft is taken by sliding over  #
        # step number of samples such that the overlap between   #
        # adjacent ffts is fft_len minus step.                   #
        # ###################################################### #
        
    
        '''
        
        buf = observation.receive[self.Detector_Channel]**2
        fft_len = self.N_OBS
        step = self.N_SKIP
        fft_total_len = self.FFT_SIZE
        
        padding = fft_total_len - fft_len
    
    
        RDI_map = np.zeros(((fft_total_len)/2+1,(np.size(buf)-fft_len)/step))
        
        for i in np.arange((np.size(buf)-fft_len)/step):
            #f, power = periodogram()
            RDI_map[:,i]=abs(np.fft.rfft((np.append(buf[i*step:i*step+fft_len],np.zeros(padding))))/(fft_total_len))**2
        return RDI_map
    
    def sam2range(self, sample):
        Range = (sample)*baud_rate*c/2 
        return Range
    
    def range2sam(self, Range):
        sample = 2*Range/c/baud_rate
        return sample
     
    def max_range_freq(self, RDI):
        f_sample, range_sample = np.unravel_index(np.argmax(RDI[:][1:-1]), RDI.shape)
        Range = self.sam2range(range_sample + blank_length)
        
        frequency = 10.**6/(self.FFT_SIZE/2)*(f_sample+1)-0.5*10**6
        return frequency/10**3, Range/10**3
    
    def plot_RDI(self, observation):
        
        fig, ax = plt.subplots()#figsize=(11.,8.5))
        RDI = self.RDI(observation)

        spacing = 100000
        f_labels = np.round(np.arange(-1.0/(4.0*baud_rate), 1.0/(4.0*baud_rate) + spacing, spacing)/10**3)#*c/2/f_0)
        f_ticks  = np.linspace(0, self.FFT_SIZE/2, len(f_labels))
        
        spacing = 150000./2
        range_labels = np.arange(self.sam2range(blank_length), self.sam2range(PRI_length-pulse_length), spacing)/10**3 + 6.25
        range_ticks  = np.linspace(self.range2sam(6.25*10**3), (PRI_length-blank_length-pulse_length)/self.N_SKIP + self.range2sam(6.25*10**3), len(range_labels))
        ax.imshow(RDI[1:-1], aspect = 'auto')

        plt.yticks(f_ticks, f_labels)
        plt.xticks(range_ticks, range_labels)
        plt.xlabel("Range (km)")
        plt.ylabel("Frequency (kHz)")
        #plt.ylabel("Range-Rate (km/s)")
        
        return fig, ax
        
if __name__ == "__main__":
    
    Detector().plot_RDI(blah)
    print Detector().max_range_freq(RDI)
        
        
        
        