import Tkinter
import tkMessageBox
import numpy as np
import scipy.special as sps
import scipy.integrate as integrate

speed_of_light = 29979245800.0 #Units = cm/s

top = Tkinter.Tk()

Parameters = {}
P_names = {}


Parameters['PatchLength'] = {}
Parameters['PatchWidth']  = {}
Parameters['PatchHeight'] = {}
Parameters['DielectricConstant'] = {}
Parameters['ResonantFrequency']  = {}
Parameters['Dielec_effective'] = {}
Parameters['FeedLocation50'] = {}
Parameters['FeedLocation'] = {}
Parameters['EdgeImpeadance'] = {}
Parameters['Impeadance'] = {}
Parameters['AbsorbedPower'] = {}

Parameters['PatchLength']['value'] = 0.906
Parameters['PatchWidth']['value']  = 0.467
Parameters['PatchHeight']['value'] = 0.1588
Parameters['DielectricConstant']['value'] = 2.2
Parameters['ResonantFrequency']['value']  = 10
Parameters['Dielec_effective']['value'] = 1.972
Parameters['FeedLocation50']['value'] = 0
Parameters['FeedLocation']['value'] = 0
Parameters['EdgeImpeadance']['value'] = 0
Parameters['Impeadance']['value'] = 0
Parameters['AbsorbedPower']['value'] = 0

Parameters['PatchLength']['name'] = "Patch Length (cm)"
Parameters['PatchWidth']['name']  = "Patch Width (cm)"
Parameters['PatchHeight']['name'] = "Patch Height (cm)"
Parameters['DielectricConstant']['name'] = "Dielectric Constant"
Parameters['ResonantFrequency']['name']  = "Resonant Frequency (GHz)"
Parameters['Dielec_effective']['name'] = "Effective Dielectric Constant"
Parameters['FeedLocation50']['name'] = "50 Ohms Feed Location (cm)"
Parameters['FeedLocation']['name'] = "Feed Location (cm)"
Parameters['EdgeImpeadance']['name'] = "Edge Impeadance at Resonant Frequency (Ohms)"
Parameters['Impeadance']['name'] = "Feed Impeadance at Resonant Frequency (Ohms)"
Parameters['AbsorbedPower']['name'] = "Absorbed Power at Feed Location (%)"

Parameters['PatchLength']['row'] = 1
Parameters['PatchWidth']['row']  = 2
Parameters['PatchHeight']['row'] = 3
Parameters['DielectricConstant']['row'] = 4
Parameters['ResonantFrequency']['row']  = 5
Parameters['Dielec_effective']['row'] = 6
Parameters['FeedLocation50']['row'] = 7
Parameters['FeedLocation']['row'] = 8
Parameters['EdgeImpeadance']['row'] = 9
Parameters['Impeadance']['row'] = 10
Parameters['AbsorbedPower']['row'] = 11

Parameters['PatchLength']['input'] = False
Parameters['PatchWidth']['input']  = False
Parameters['PatchHeight']['input'] = False
Parameters['DielectricConstant']['input'] = False
Parameters['ResonantFrequency']['input']  = False
Parameters['Dielec_effective']['input'] = False
Parameters['FeedLocation50']['input'] = False
Parameters['FeedLocation']['input'] = False
Parameters['EdgeImpeadance']['input'] = False
Parameters['Impeadance']['input'] = False
Parameters['AbsorbedPower']['input'] = False

def quitbutton():
    exit()

def calc_absorbed_power():

    get_inputs()

    Z_feed = Parameters['Impeadance']['value']

    Parameters['AbsorbedPower']['value'] = 1 - (Z_feed - 50.0)**2/(Z_feed + 50.0)**2


def calc_patch_geometry():

    get_inputs()

    er = Parameters['DielectricConstant']['value']
    h  = Parameters['PatchHeight']['value']
    fr = Parameters['ResonantFrequency']['value']

    W = speed_of_light/(fr*1e9)/np.sqrt(2*(er+1))

    er_eff = (er+1)/2 + (er-1)/2/np.sqrt((1 + 12*h/W))
    
    deltaL = h * 0.412*(er_eff + 0.3)*(W/h + 0.264)/(er_eff - 0.258)/(W/h + 0.8)

    L = speed_of_light/(fr*1e9*np.sqrt(4*er_eff)) - 2*deltaL

    print W
    print er_eff
    print deltaL
    print L

    Parameters['PatchWidth']['value'] = W
    Parameters['PatchLength']['value']= L
    Parameters['Dielec_effective']['value'] = er_eff

    calc_impeadance()

def calc_impeadance():

    get_inputs()

    er = Parameters['DielectricConstant']['value']
    h  = Parameters['PatchHeight']['value']
    fr = Parameters['ResonantFrequency']['value']
    W  = Parameters['PatchWidth']['value'] 
    L  = Parameters['PatchLength']['value']
    er_eff = Parameters['Dielec_effective']['value']
    y0 = Parameters['FeedLocation']['value']


    X = 2*np.pi*fr*1e9*W/speed_of_light

    I1 = -2 + np.cos(X) + X*sps.sici(X)[0] + np.sin(X)/X

    G1 = I1/(120*np.pi**2)

    I12 = integrate.quad(lambda t: ((np.sin(X*np.cos(t)/2)/np.cos(t))**2) * sps.jv(0,X*L/W*np.sin(t)) * np.sin(t)**3,0,np.pi)[0]
    G12 = I12/(120*np.pi**2)

    Rin = 1.0/(2*(G1 + G12))

    y0_50 = np.arccos(np.sqrt((50.0/Rin)))*L/np.pi

    print X
    print I1
    print G1
    print G12
    print Rin
    print y0_50
    
    Parameters['EdgeImpeadance']['value'] = Rin
    Parameters['Impeadance']['value'] = Rin*np.cos(np.pi*y0/L)**2
    Parameters['FeedLocation50']['value'] = y0_50

def setup_inputs():

    for x in Parameters:
        if(Parameters[x]['input']):
            default = Tkinter.StringVar(top,value=np.str(Parameters[x]['value']))
            L = Tkinter.Label(top,text=Parameters[x]['name'])
            E = Tkinter.Entry(top,bd=5,textvariable=default)
            L.grid(row=Parameters[x]['row'],column=0)
            E.grid(row=Parameters[x]['row'],column=1)
            Parameters[x]['entries'] = E

        else:
            L1 = Tkinter.Label(top,text=Parameters[x]['name'])
#            L2 = Tkinter.Label(top,text=np.str(Parameters[x]['value']))
            default = Tkinter.StringVar(top,value=np.str(Parameters[x]['value']))
            L2 = Tkinter.Entry(top,bd=5,textvariable=default,state=Tkinter.DISABLED)
            L1.grid(row=Parameters[x]['row'],column=0)
            L2.grid(row=Parameters[x]['row'],column=1)
            Parameters[x]['label'] = L2

def forget():
    for x in Parameters:
        if(Parameters[x]['input']):
            Parameters[x]['entries'].grid_forget()
        else:
            try:
                print "Removing Label"
                Parameters[x]['label'].grid_forget()
                break
            except:
                print "Error Removing Label"
                pass
            
def setup_patch_geometry():


    calc_patch_geometry()
    calc_absorbed_power()
    
    forget()
    Parameters['PatchLength']['input'] = False
    Parameters['PatchWidth']['input']  = False
    Parameters['PatchHeight']['input'] = True
    Parameters['DielectricConstant']['input'] = True
    Parameters['ResonantFrequency']['input']  = True
    Parameters['FeedLocation']['input'] = True

    setup_inputs()

def setup_resonant_freq():

    forget()
    Parameters['PatchLength']['input'] = True
    Parameters['PatchWidth']['input']  = True
    Parameters['PatchHeight']['input'] = True
    Parameters['DielectricConstant']['input'] = True
    Parameters['ResonantFrequency']['input']  = False

    setup_inputs()

def get_inputs():

    for x in Parameters:
        if(Parameters[x]['input']):
            Parameters[x]['value']=np.float(Parameters[x]['entries'].get())
                                            
#def calculate_patch_geometry()

B1 = Tkinter.Button(top,text="Calculate Patch Geometry",command = setup_patch_geometry)
B2 = Tkinter.Button(top,text="Calculate Resonant Frequency",command = setup_resonant_freq)
B1.grid(row=0,column=0)
B2.grid(row=0,column=1)

setup_inputs()

    
top.mainloop()

